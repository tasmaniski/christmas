<?php

/**
 * Make path resolving easyer for web and console too
 */
chdir(dirname(__DIR__));

/**
 * Composer autoload
 */
require 'vendor/autoload.php';

/**
 * Check if we call app from the console or from the web
 */
$cli = (php_sapi_name() == "cli");

/**
 * Application logic
 */
try {
    // Generate one tree output
    $tree       = new \Shapes\Tree();
    $treeOutput = $tree->generate();

    // Generate one star output
    $star       = new \Shapes\Star();
    $starOutput = $star->generate();

    // Output
    echo $cli ? '' : '<pre>';
    echo $treeOutput;
    echo $cli ? "\r\n" : '</pre>';

    echo $cli ? '' : '<pre>';
    echo $starOutput;
    echo $cli ? "\r\n" : '</pre>';
}
catch (\Exception $e) {
    echo $e->getMessage();
}