<?php

namespace Shapes;

class Tree extends AbstractShape
{
    /**
     * Generate the ASCII tree
     *
     * @return string ASCII tree shape to write to the output
     */
    public function generate()
    {
        $spaces = $this->getSpaces();
        $chars  = $this->getChars();
        $tree   = str_repeat(' ', $spaces) . '+' . str_repeat(' ', $spaces) . "\r\n";

        for ($i = $spaces; $i >= 0; $i--) {
            $tree .= str_repeat(' ', $i) .
                str_repeat('x', $chars - (2 * $i)) .
                str_repeat(' ', $i) . "\r\n";
        }

        return $tree;
    }

    /**
     * Return number of spaces for specific size
     *
     * @return int
     */
    private function getSpaces()
    {
        $spaces = ['S' => 3, 'M' => 5, 'L' => 9];

        return $spaces[$this->getSize()];
    }

    /**
     * Return max characters in one line for specific size
     *
     * @return int
     */
    private function getChars()
    {
        $a = ['S' => 7, 'M' => 11, 'L' => 19];

        return $a[$this->getSize()];
    }

}