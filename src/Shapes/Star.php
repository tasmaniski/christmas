<?php

namespace Shapes;

class Star extends AbstractShape
{
    const S = '
   +
   x
+xxxxx+
   x
   +
   ';

    const M = '
     +
     x
   xxxxx
+xxxxxxxxx+
   xxxxx
     x
     +
     ';

    const L = '
        +
        x
       xxx
     xxxxxxx
   xxxxxxxxxxx
+xxxxxxxxxxxxxxx+
   xxxxxxxxxxx
     xxxxxxx
       xxx
        x
        +
        ';

    /**
     * Create start from ready "templates"
     *
     * @return string
     * @throws \Exception
     */
    public function generate()
    {
        if($this->getSize() === 'S'){
            return self::S;
        }
        elseif($this->getSize() === 'M'){
            return self::M;
        }
        elseif($this->getSize() === 'L'){
            return self::L;
        }

        throw new \Exception('Invalid size', 400);
    }
}