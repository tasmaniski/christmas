<?php

namespace Shapes;

abstract class AbstractShape
{
    private $size;

    const AVAILABLE_SIZES = ['S' => 5, 'M' => 7, 'L' => 11];

    /**
     * AbstractShape constructor.
     *
     * @param null|char $size Accepted values are: S, M or L
     *
     * @throws \Exception
     */
    public function __construct($size = null)
    {
        if ($size && !array_key_exists($size, self::AVAILABLE_SIZES)) {
            throw new \Exception('Please choose size: S, M or L.', 400);
        }

        if (!$size) {
            $sizes = array_keys(self::AVAILABLE_SIZES);
            shuffle($sizes);
            $size = array_shift($sizes);
        }

        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }

    abstract public function generate();
}