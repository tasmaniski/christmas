<?php

namespace Shapes;

class TreeTest extends \PHPUnit_Framework_TestCase
{
    public function testSSize()
    {
        $trueOutput =
'   +   ' . "\r\n" .
'   x   ' . "\r\n" .
'  xxx  ' . "\r\n" .
' xxxxx ' . "\r\n" .
'xxxxxxx' . "\r\n";

        $star   = new \Shapes\Tree('S');
        $output = $star->generate();
        $equal  = ($output == $trueOutput) ;
        $this->assertTrue($equal);
    }

    public function testMSize()
    {
        $trueOutput =
'     +     ' . "\r\n" .
'     x     ' . "\r\n" .
'    xxx    ' . "\r\n" .
'   xxxxx   ' . "\r\n" .
'  xxxxxxx  ' . "\r\n" .
' xxxxxxxxx ' . "\r\n" .
'xxxxxxxxxxx' . "\r\n";

        $star   = new \Shapes\Tree('M');
        $output = $star->generate();
        $equal  = ($output == $trueOutput) ;
        $this->assertTrue($equal);
    }

    public function testLSize()
    {
        $trueOutput =
'         +         ' . "\r\n" .
'         x         ' . "\r\n" .
'        xxx        ' . "\r\n" .
'       xxxxx       ' . "\r\n" .
'      xxxxxxx      ' . "\r\n" .
'     xxxxxxxxx     ' . "\r\n" .
'    xxxxxxxxxxx    ' . "\r\n" .
'   xxxxxxxxxxxxx   ' . "\r\n" .
'  xxxxxxxxxxxxxxx  ' . "\r\n" .
' xxxxxxxxxxxxxxxxx ' . "\r\n" .
'xxxxxxxxxxxxxxxxxxx' . "\r\n";

        $star   = new \Shapes\Tree('L');
        $output = $star->generate();
        $equal  = ($output == $trueOutput) ;
        $this->assertTrue($equal);
    }
}