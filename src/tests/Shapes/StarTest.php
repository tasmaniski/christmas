<?php

namespace Shapes;

/**
 * Class StarTest
 *
 * @package Shapes
 */
class StarTest extends \PHPUnit_Framework_TestCase
{

    public function testSSize()
    {
        $trueOutput = '
   +
   x
+xxxxx+
   x
   +
   ';
        $star   = new \Shapes\Star('S');
        $output = $star->generate();
        $equal  = ($output == $trueOutput) ;
        $this->assertTrue($equal);
    }

    public function testMSize()
    {
        $trueOutput = '
     +
     x
   xxxxx
+xxxxxxxxx+
   xxxxx
     x
     +
     ';

        $star   = new \Shapes\Star('M');
        $output = $star->generate();
        $equal  = ($output == $trueOutput) ;
        $this->assertTrue($equal);
    }

    public function testLSize()
    {
        $trueOutput = '
        +
        x
       xxx
     xxxxxxx
   xxxxxxxxxxx
+xxxxxxxxxxxxxxx+
   xxxxxxxxxxx
     xxxxxxx
       xxx
        x
        +
        ';

        $star   = new \Shapes\Star('L');
        $output = $star->generate();
        $equal  = ($output == $trueOutput) ;
        $this->assertTrue($equal);
    }

}
