# README #

In this repository you can find finished task.

There is small bootstrap.sh script just to install basics:

	PHP 7.0
	Nginx

### (Install) To run the task you need: ###
	cd /var/www/
	git clone git@bitbucket.org:tasmaniski/christmas.git
	cd christmas
	vagrant up (or check bootstrap.sh)
	add one line in /etc/hosts/ >> 192.168.33.9 christmas.dev

### Run the task ###

After this steps:

    open in your browser christmas.dev
    run through command line: php /var/www/christmas/public/index.php

In booth way you will see the output - one random generated tree and one star.

### Unit tests ###
    cd /var/www/christmas
    vendor/bin/phpunit src/tests/


