#!/usr/bin/env bash


echo "
────────────────────────────────────────────────────────────────────────────────────────────────────────
   Update and install all tools
────────────────────────────────────────────────────────────────────────────────────────────────────────"
sudo add-apt-repository ppa:ondrej/php      >> /dev/null 2>&1
sudo apt-get update                         >> /dev/null 2>&1

echo "
────────────────────────────────────────────────────────────────────────────────────────────────────────
   Install PHP 7.0 and rest packages
────────────────────────────────────────────────────────────────────────────────────────────────────────"
sudo apt-get install -y nginx       >> /dev/null 2>&1
sudo apt-get install -y curl git    >> /dev/null 2>&1
sudo apt-get install -y php7.0-zip php7.0-fpm php7.0-mcrypt php7.0-curl php7.0-cli php7.0-mysql php7.0-gd php7.0-intl php7.0-xsl php7.0-mbstring >> /dev/null 2>&1
sudo apt-get install -y php-xdebug  >> /dev/null 2>&1


echo "
────────────────────────────────────────────────────────────────────────────────────────────────────────
   Install and run composer
────────────────────────────────────────────────────────────────────────────────────────────────────────"
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024   >> /dev/null 2>&1
sudo /sbin/mkswap /var/swap.1                               >> /dev/null 2>&1
sudo /sbin/swapon /var/swap.1                               >> /dev/null 2>&1

cd /tmp/
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"   >> /dev/null 2>&1
php composer-setup.php                                                      >> /dev/null 2>&1
php -r "unlink('composer-setup.php');"
sudo mv composer.phar /usr/local/bin/composer
cd /var/www/christmas/

if [ -d "vendor" ]; then
 composer update      >> /dev/null 2>&1
else
 composer install     >> /dev/null 2>&1
fi

echo "
────────────────────────────────────────────────────────────────────────────────────────────────────────
   Setup Nginx vhost
────────────────────────────────────────────────────────────────────────────────────────────────────────"
sudo touch /etc/nginx/sites-available/christmas.conf
sudo bash -c "echo 'server {
    listen 80;
    listen [::]:80;
    root /var/www/christmas/public;
    index index.php index.html index.htm;
    server_name christmas.dev;
    location / {
        root /var/www/christmas/public;
        index  index.html index.htm index.php;
        try_files \$uri \$uri/ /index.php?\$args;
    }

    error_page 404 /404.html;
    error_page 500 502 503 504 /50x.html;
    location = /50x.html {
        root /usr/share/nginx/html;
    }

    location ~ \.php$ {
        try_files \$uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.0-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        include fastcgi_params;
    }
}' > /etc/nginx/sites-available/christmas.conf"

sudo cp /etc/nginx/sites-available/christmas.conf /etc/nginx/sites-enabled/
sudo service nginx restart          >> /dev/null 2>&1
sudo service php7.0-fpm restart     >> /dev/null 2>&1

echo "
────────────────────────────────────────────────────────────────────────────────────────────────────────
   Almost done,
   In your /etc/hosts file add line
   192.168.33.3 christmas.dev
────────────────────────────────────────────────────────────────────────────────────────────────────────"